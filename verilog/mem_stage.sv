/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  mem_stage.v                                         //
//  Description : memory access (MEM) stage of the pipeline;           //
//                this stage acccesses memory for stores and loads,    //
//                and selects the proper next PC value for branches    //
//                based on the branch condition computed in the        //
//                previous stage.                                      //
//                                                                     //
//                                                                     //
/////////////////////////////////////////////////////////////////////////


`ifndef __MEM_STAGE_V__
`define __MEM_STAGE_V__

`timescale 1ns/100ps

/*
typedef struct packed {
	logic [`XLEN-1:0] alu_result; // alu_result
	logic [`XLEN-1:0] NPC; //pc + 4
	logic             take_branch; // is this a taken branch?
	//pass throughs from decode stage
	logic [`XLEN-1:0] rs2_value;
	logic             rd_mem, wr_mem; // Tell us whether is load or store
	logic [4:0]       dest_reg_idx;
	logic             halt, illegal, csr_op, valid;
	logic [2:0]       mem_size; // byte, half-word or word
} EX_MEM_PACKET;

typedef enum logic [1:0] {
	BYTE = 2'h0,
	HALF = 2'h1,
	WORD = 2'h2,
	DOUBLE = 2'h3
} MEM_SIZE;
*/

module mem_stage(
        input clock,        // system clock
        input reset,        // system reset
        input EX_MEM_PACKET ex_mem_packet_in,           // Tells us how to deal with memory (via rd_mem, wr_mem)
        input   [`XLEN-1:0] Dmem2proc_data,

        output logic [`XLEN-1:0] mem_result_out,        // outgoing instruction result (to MEM/WB)
        output logic [1:0] proc2Dmem_command, 
        output MEM_SIZE proc2Dmem_size,
        output logic [`XLEN-1:0] proc2Dmem_addr,        // Address sent to data-memory
        output logic [`XLEN-1:0] proc2Dmem_data         // Data sent to data-memory
);


    // Determine the command that must be sent to mem
    assign proc2Dmem_command =
                            ( ex_mem_packet_in.wr_mem & ex_mem_packet_in.valid ) ? BUS_STORE :
                            ( ex_mem_packet_in.rd_mem & ex_mem_packet_in.valid ) ? BUS_LOAD :
                            BUS_NONE; // If this is not a memory-related instr (allow instruction loads to occur)
    
    assign proc2Dmem_size = MEM_SIZE'( ex_mem_packet_in.mem_size[1:0] ); // only use the 2 LSB to determine size 
    // The 3rd bit is used to determine signedness



    // The memory address is calculated by the ALU
    assign proc2Dmem_addr = ex_mem_packet_in.alu_result;

    assign proc2Dmem_data = ex_mem_packet_in.rs2_value; // For stores (only) -> Will be ignored by memory_module o.w.
    
    // Assign the result-out for next_stage
    always_comb
    begin
            mem_result_out = ex_mem_packet_in.alu_result; // Initally points to alu result -> Should be the case for an instruction not memory related.
            if ( ex_mem_packet_in.rd_mem )
            begin
                if ( ~ex_mem_packet_in.mem_size[ 2 ] ) // The 3rd bit means that this is signed (if 0) -> Signed load
                begin
                        if ( ex_mem_packet_in.mem_size[1:0] == BYTE )
                            mem_result_out = { {(`XLEN-8){ Dmem2proc_data[ 7 ] }}, Dmem2proc_data[7:0] }; // Bit extends the 7th bit (up to XLEN-1)
                        else if ( ex_mem_packet_in.mem_size[1:0] == HALF )
                            mem_result_out = { {(`XLEN-16){ Dmem2proc_data[ 15 ] }}, Dmem2proc_data[15:0] };
                        else // WORD
                            mem_result_out = Dmem2proc_data;
                end // if ( ~ex_mem_packet_in.mem_size[ 2 ] )
                else // Unsigned load
                begin
                    if ( ex_mem_packet_in.mem_size[1:0] == BYTE )
                            mem_result_out = { {(`XLEN-8){1'b0}}, Dmem2proc_data[7:0] };
                    else if ( ex_mem_packet_in.mem_size[1:0] == HALF )
                            mem_result_out = { {(`XLEN-16){1'b0}}, Dmem2proc_data[15:0] };
                    else // WORD
                            mem_result_out = Dmem2proc_data;
                end // else -> ( ex_mem_packet_in.mem_size[ 2 ] == 1'b1 )
            end // if ( ex_mem_packet_in.rd_mem )
    end // always_comb

    // if we are in 32-bit mode, then we should never load a double word sized data
    assert property ( @( negedge clock ) ( `XLEN == 32 ) && ex_mem_packet_in.rd_mem |-> proc2Dmem_size != DOUBLE );
    
endmodule // mem_stage
`endif // __MEM_STAGE_V__
