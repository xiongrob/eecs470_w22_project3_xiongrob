//////////////////////////////////////////////////////////////////////////
//                                                                      //
//   Module name : ex_stage.v                                           //
//   Description : instruction execute (EX) stage of the pipeline;      //
//                 given the instruction command code CMB, select       //
//                 the proper input A and B for ALU, compute the        //
//                 results, and compute the coniditon for branches,     //
//                 and pass all the resutls down the pipeline. MWB      //
//                                                                      //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
`ifndef __EX_STAGE_V__
`define __EX_STAGE_V__

`timescale 1ns/100ps

//
// The ALU
//
// given the command code CMD and proper operands A and B, compute the
// result of the instruction
//
// This module is purely combinational
//

/*
typedef enum logic [4:0] {
    ALU_ADD     = 5'h00,
    ALU_SUB     = 4'h01,
    ALU_SLT     = 5'h02,
    ALU_SLTU    = 5'h03,
	ALU_AND     = 5'h04,
	ALU_OR      = 5'h05,
	ALU_XOR     = 5'h06,
	ALU_SLL     = 5'h07,
	ALU_SRL     = 5'h08,
	ALU_SRA     = 5'h09,
	ALU_MUL     = 5'h0a,
	ALU_MULH    = 5'h0b,
	ALU_MULHSU  = 5'h0c,
	ALU_MULHU   = 5'h0d,
	ALU_DIV     = 5'h0e,
	ALU_DIVU    = 5'h0f,
	ALU_REM     = 5'h10,
	ALU_REMU    = 5'h11
} ALU_FUNC;
*/

module alu(
        input [`XLEN-1:0] opa,
        input [`XLEN-1:0] opb,
        ALU_FUNC            func,

        output logic [`XLEN-1:0] result
);
    wire signed [`XLEN-1:0] signed_opa, signed_opb; 
    wire signed [2*`XLEN-1:0] signed_mul, mixed_mul;
    wire        [2*`XLEN-1:0] unsigned_mul;

    assign signed_opa = opa; // This is a signed type so it seems that sign extension will automatically be done for us?
    assign signed_opb = opb;
    assign signed_mul = signed_opa * signed_opb;
    assign unsigned_mul = opa * opb;
    assign mixed_mul = signed_opa * opb;

    always_comb
    begin
            case( func ) // utlizes enum from ALU_FUNC
                    ALU_ADD:    result = opa + opb;
                    ALU_SUB:    result = opa - opb;
                    ALU_AND:    result = opa & opb;               
                    ALU_SLT:    result = signed_opa < signed_opb; // Signed less-than
                    ALU_SLTU:   result = opa < opb;
                    ALU_OR:     result = opa | opb;
                    ALU_XOR:    result = opa ^ opb;
                    ALU_SRL:    result = opa >> opb[4:0];         // Shift Right Logical
                    ALU_SLL:    result = opa << opb[4:0];         // Shift Left Logical
                    ALU_SRA:    result = signed_opa >>> opb[4:0]; // arithmetic from logical shift
                    ALU_MUL:    result = signed_mul[`XLEN-1:0];
                    ALU_MULH:   result = signed_mul[2*`XLEN-1:`XLEN];
                    ALU_MULHSU: result = mixed_mul[2*`XLEN-1:`XLEN];
                    ALU_MULHU:  result = unsigned_mul[2*`XLEN-1:`XLEN];
                    default:    result = `XLEN'hfacebeec;   // here to prevent latches
            endcase
    end // always_comb
endmodule // alu


//
// BrCond module
//
// Given the instruction code, compute the proper condition for the
// insturction; for branches this condition will indicate whether the target
// is taken.
//
// Like the alu, this module is purely combinational
//

module brcond( // Inputs
        input [`XLEN-1:0] rs1,      // Value to check against condition
        input [`XLEN-1:0] rs2,
        input [2:0] func, // Specifies which condition to check

        output logic cond
);

    logic signed [`XLEN-1:0] signed_rs1, signed_rs2;
    assign signed_rs1 = rs1;
    assign signed_rs2 = rs2;

    always_comb 
    begin
            cond = 0;
            case ( func )
                    3'b000: cond = signed_rs1 == signed_rs2;    // BEQ
                    3'b001: cond = signed_rs1 != signed_rs2;    // BNE
                    3'b100: cond = signed_rs1 <  signed_rs2;    // BLT
                    3'b101: cond = signed_rs1 >= signed_rs2;    // BGE
                    3'b110: cond = rs1 < rs2;                   // BLTU (unsigned)
                    3'b111: cond = rs1 >= rs2;                  // BGEU (unsigned)
            endcase // ( func )
    end // always_comb

endmodule // brcond

//`define RV32_signext_Iimm(instr) {{21{``instr``[31]}},``instr``[30:20]}
/*
//
// ALU opA input mux selects
//
typedef enum logic [1:0] {
	OPA_IS_RS1  = 2'h0,
	OPA_IS_NPC  = 2'h1,
	OPA_IS_PC   = 2'h2,
	OPA_IS_ZERO = 2'h3
} ALU_OPA_SELECT;

//
// ALU opB input mux selects
//
typedef enum logic [3:0] {
	OPB_IS_RS2    = 4'h0,
	OPB_IS_I_IMM  = 4'h1,
	OPB_IS_S_IMM  = 4'h2,
	OPB_IS_B_IMM  = 4'h3,
	OPB_IS_U_IMM  = 4'h4,
	OPB_IS_J_IMM  = 4'h5
} ALU_OPB_SELECT;

typedef struct packed {
	logic [`XLEN-1:0] NPC;   // PC + 4
	logic [`XLEN-1:0] PC;    // PC

	logic [`XLEN-1:0] rs1_value;    // reg A value                                  
	logic [`XLEN-1:0] rs2_value;    // reg B value                                  
	                                                                                
	ALU_OPA_SELECT opa_select; // ALU opa mux select (ALU_OPA_xxx *)
	ALU_OPB_SELECT opb_select; // ALU opb mux select (ALU_OPB_xxx *)
	INST inst;                 // instruction
	
	logic [4:0] dest_reg_idx;  // destination (writeback) register index      
	ALU_FUNC    alu_func;      // ALU function select (ALU_xxx *)
	logic       rd_mem;        // does inst read memory?
	logic       wr_mem;        // does inst write memory?
	logic       cond_branch;   // is inst a conditional branch?
	logic       uncond_branch; // is inst an unconditional branch?
	logic       halt;          // is this a halt?
	logic       illegal;       // is this instruction illegal?
	logic       csr_op;        // is this a CSR operation? (we only used this as a cheap way to get return code)
	logic       valid;         // is inst a valid instruction to be counted for CPI calculations?
} ID_EX_PACKET;

typedef struct packed {
	logic [`XLEN-1:0] alu_result; // alu_result
	logic [`XLEN-1:0] NPC; //pc + 4
	logic             take_branch; // is this a taken branch?
	//pass throughs from decode stage
	logic [`XLEN-1:0] rs2_value;
	logic             rd_mem, wr_mem;
	pogic [4:0]       dest_reg_idx;
	logic             halt, illegal, csr_op, valid;
	logic [2:0]       mem_size; // byte, half-word or word
} EX_MEM_PACKET;
*/

module bypass_mux (
        input BYPASS_SELECT sel,
        input [`XLEN-1:0] rs_val, ex_mem_frwd, mem_wb_frwd,
        output logic [`XLEN-1:0] final_rs_val
);
    always_comb
    begin
            final_rs_val = rs_val;
            case ( sel )
                NO_BYPASS:      final_rs_val = rs_val;
                FRWD_EX_MEM:    final_rs_val = ex_mem_frwd;
                FRWD_MEM_EX:    final_rs_val = mem_wb_frwd;
            endcase // case ( sel )
    end
endmodule

module ex_stage(
        input clock,        // system clock
        input reset,
        input ID_EX_PACKET  id_ex_packet_in,
        input [`XLEN-1:0] ex_mem_fwrd, mem_wb_fwrd,
        output EX_MEM_PACKET ex_packet_out
        /*
        input frwd_rs1_val, frwd_rs1_val // Used to bring (values from either the mem_stage or wb_stage)
        */
);
    // Wires used
    logic [`XLEN-1:0] opa_mux_out, opb_mux_out; // Used as inputs (results from muxes applied)
    logic [`XLEN-1:0] frwd_rs1, frwd_rs2; // Forward values
    logic brcond_result; // Wire that helps to determine whether or not to branch.

    // Pass-throughs
    assign ex_packet_out.NPC = id_ex_packet_in.NPC;

    //! This needs to be potentially fowarded from wb_stage (esp in the case
    //of a load that is subsequently used as a store)
    assign ex_packet_out.rs2_value      = frwd_rs2;
    assign ex_packet_out.rd_mem         = id_ex_packet_in.rd_mem;               // Does this instruction plan on reading memory?
    assign ex_packet_out.wr_mem         = id_ex_packet_in.wr_mem;               // Does this instruction plan on writing to memory?
    assign ex_packet_out.dest_reg_idx   = id_ex_packet_in.dest_reg_idx;         // What is the index of the dest register?
    assign ex_packet_out.halt           = id_ex_packet_in.halt;                 // Will it halt?
    assign ex_packet_out.illegal        = id_ex_packet_in.illegal;              // Instruction illegal? (should cause an exception)
    assign ex_packet_out.csr_op         = id_ex_packet_in.csr_op;           
    assign ex_packet_out.valid          = id_ex_packet_in.valid;                // valid inxtruction (ow treat like a noop)
    assign ex_packet_out.mem_size       = id_ex_packet_in.inst.r.funct3;

    // Instantiate Mux values
    bypass_mux rsa( .sel( id_ex_packet_in.frwda_sel ), 
                    .rs_val( id_ex_packet_in.rs1_value ), 
                    .ex_mem_frwd( ex_mem_fwrd  ), 
                    .mem_wb_frwd( mem_wb_fwrd ), 
                    .final_rs_val( frwd_rs1 ) );
    
    bypass_mux rsb( .sel( id_ex_packet_in.frwdb_sel ), 
                    .rs_val( id_ex_packet_in.rs2_value ), 
                    .ex_mem_frwd( ex_mem_fwrd ), 
                    .mem_wb_frwd( mem_wb_fwrd ), 
                    .final_rs_val( frwd_rs2 ) );
    //
    // ALU opA mux
    //
    always_comb
    begin
            opa_mux_out = `XLEN'hdeadfbac;
            case ( id_ex_packet_in.opa_select )
                OPA_IS_RS1:     opa_mux_out = frwd_rs1;
                OPA_IS_NPC:     opa_mux_out = id_ex_packet_in.NPC;
                OPA_IS_PC:      opa_mux_out = id_ex_packet_in.PC;
                OPA_IS_ZERO:    opa_mux_out = 0;
            endcase // ( id_ex_packet_in.opa_select )
    end // always_comb      

    //
    // ALU opb mux
    //
    always_comb
    begin
            opb_mux_out = `XLEN'hfacefeed;

            case ( id_ex_packet_in.opb_select )
                OPB_IS_RS2:     opb_mux_out = frwd_rs2;
                OPB_IS_I_IMM:   opb_mux_out = `RV32_signext_Iimm( id_ex_packet_in.inst );
                OPB_IS_S_IMM:   opb_mux_out = `RV32_signext_Simm( id_ex_packet_in.inst );
                OPB_IS_B_IMM:   opb_mux_out = `RV32_signext_Bimm( id_ex_packet_in.inst );
                OPB_IS_U_IMM:   opb_mux_out = `RV32_signext_Uimm( id_ex_packet_in.inst );
                OPB_IS_J_IMM:   opb_mux_out = `RV32_signext_Jimm( id_ex_packet_in.inst );
            endcase // ( id_ex_packet_in.opb_select )
    end // always_comb

    //
    // Instantiate the ALU
    //
    alu alu_0 ( // Inputs
            .opa( opa_mux_out ),
            .opb( opb_mux_out ),
            .func( id_ex_packet_in.alu_func ),

            // Output
            .result( ex_packet_out.alu_result )
    );

    // Sort of the other part of the ALU (used to just figure out condition)
    brcond brcond ( // Inpust
            .rs1( frwd_rs1 ),
            .rs2( frwd_rs2 ),
            .func( id_ex_packet_in.inst.b.funct3 ),

            // Output
            .cond( brcond_result )
    );
    // ultimate "take branch" signal:
    // unconditional, or conditional and the condition is true (and whether or
    // not branch is even valid)
    assign ex_packet_out.take_branch = id_ex_packet_in.uncond_branch | ( id_ex_packet_in.cond_branch & brcond_result & id_ex_packet_in.valid );
endmodule // ex_stage
`endif // __EX_STAGE_V__
