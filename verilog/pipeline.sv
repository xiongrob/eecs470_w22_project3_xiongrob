

// My implementation of the piepline.sv


// Top-level module of the verisimple pipeline.

`ifndef __PIPELINE_V__
`define __PIPELINE_V__


`timescale 1ns/100ps


// `define XLEN32


/*
tyedef enum logic [1:0] {
        BYTE = 2'h0,
    HALF = 2'h1,
    WORD = 2'h2,
    DOUBLE = 2'h3
} MEM_SIZE;

// Holds information like what the pc / next pc are
typedef struct packed {
    logic valid; // If low, the data in this struct is garbage
    INST  inst;  // fetched instruction out
    logic [`XLEN-1:0] NPC; // PC + 4
    logic [`XLEN-1:0] PC;  // PC 
} IF_ID_PACKET;

typedef struct packed {
    logic [`XLEN-1:0] NPC;   // PC + 4
    logic [`XLEN-1:0] PC;    // PC

    logic [`XLEN-1:0] rs1_value;    // reg A value                                  
    logic [`XLEN-1:0] rs2_value;    // reg B value                                  
                                                                                    
    ALU_OPA_SELECT opa_select; // ALU opa mux select (ALU_OPA_xxx *)
    ALU_OPB_SELECT opb_select; // ALU opb mux select (ALU_OPB_xxx *)
    INST inst;                 // instruction
    
    logic [4:0] dest_reg_idx;  // destination (writeback) register index      
    ALU_FUNC    alu_func;      // ALU function select (ALU_xxx *)
    logic       rd_mem;        // does inst read memory?
    logic       wr_mem;        // does inst write memory?
    logic       cond_branch;   // is inst a conditional branch?
    logic       uncond_branch; // is inst an unconditional branch?
    logic       halt;          // is this a halt?
    logic       illegal;       // is this instruction illegal?
    logic       csr_op;        // is this a CSR operation? (we only used this as a cheap way to get return code)
    logic       valid;         // is inst a valid instruction to be counted for CPI calculations?
} ID_EX_PACKET;

typedef struct packed {
    logic [`XLEN-1:0] NPC;   // PC + 4
    logic [`XLEN-1:0] PC;    // PC

    logic [`XLEN-1:0] rs1_value;    // reg A value                                  
    logic [`XLEN-1:0] rs2_value;    // reg B value                                  
                                                                                    
    ALU_OPA_SELECT opa_select; // ALU opa mux select (ALU_OPA_xxx *)
    ALU_OPB_SELECT opb_select; // ALU opb mux select (ALU_OPB_xxx *)
    INST inst;                 // instruction
    
    logic [4:0] dest_reg_idx;  // destination (writeback) register index      
    ALU_FUNC    alu_func;      // ALU function select (ALU_xxx *)
    logic       rd_mem;        // does inst read memory?
    logic       wr_mem;        // does inst write memory?
    logic       cond_branch;   // is inst a conditional branch?
    logic       uncond_branch; // is inst an unconditional branch?
    logic       halt;          // is this a halt?
    logic       illegal;       // is this instruction illegal?
    logic       csr_op;        // is this a CSR operation? (we only used this as a cheap way to get return code)
    logic       valid;         // is inst a valid instruction to be counted for CPI calculations?
} ID_EX_PACKET;

typedef struct packed {
    logic [`XLEN-1:0] alu_result; // alu_result
    logic [`XLEN-1:0] NPC; //pc + 4
    logic             take_branch; // is this a taken branch?
    //pass throughs from decode stage
    logic [`XLEN-1:0] rs2_value;
    logic             rd_mem, wr_mem;
    pogic [4:0]       dest_reg_idx;
    logic             halt, illegal, csr_op, valid;
    logic [2:0]       mem_size; // byte, half-word or word
} EX_MEM_PACKET;
*/

/*
typedef struct packed {
       logic valid; // if low, data in this struct is garbage (probably going to need to modify to implement dynamic stall)
       logic [`XLEN-1:0] NPC; // PC + 4
       logic [`XLEN-1:0] PC;  // PC
} IF_ID_PACKET;

typedef struct packed {
    logic [`XLEN-1:0] NPC; // PC + 4
    logic [`XLEN-1:0] PC;  // PC

    logic [`XLEN-1:0] rs1_value; // reg A value
    logic [`XLEN-1:0] rs2_value; // reg B value
    
    ALU_OPA_SELECT opa_select; // ALU opa mux select (ALU_OPA_xxx *)
    ALU_OPB_SELECT opb_select; // ALU opb mux select (ALU_OPB_xxx *)

    logic [4:0] dest_reg_idx;  // destination (writeback) register index
    ALU_FUNC alu_func;
    logic     rd_mem; // does inst read memory?
    logic     wr_mem; // does inst write memory?
    logic     cond_branch; // is inst write memory?
    logic     uncond_branch; // is inst an unconditional branch?
    logic     halt;      // is this a halt?
    logic     illegal;   // is this instruction illegal?
    logic     csr_op;        // is this a CSR operation? (we only used this as a cheap way to get return code)
    logic     valid;         // is inst a valid instruction to be counted for CPI calculations? (aka is the noop an actual part of the instruction (or just a stall provided).
} ID_EX_PACKET;
*/
module pipeline (
        input clock, // System clock
    input reset, // System reset
    input [3:0] mem2proc_response, // Tag from memory about current request (do we have a pagefault etc.)
    input [63:0] mem2proc_data, // Data coming back from memory
    input [3:0] mem2proc_tag,   // Tag from memory about current reply ???

    // pro2mem_command should be of type BUS_COMMAND
    // -> output BUS_COMMAND proc2mem_command,
    output logic [1:0] proc2mem_command, // command set to memory
    output logic [`XLEN-1:0] proc2mem_addr, // Address sent to memory ( look at mem.sv for details)
    output logic [63:0] proc2mem_data,
    output MEM_SIZE proc2mem_size, // data size sent to memory
    
    // Output used to give info on pipeline
    output logic [3:0] pipeline_completed_insts, // Should be incremented (each time we have an instruction reaching writeback that isn't already NOOP?)
    output EXCEPTION_CODE pipeline_error_status, // In case something goes wrong I believe (and to indicate halting) and or to tell memory cache to do something
    output logic [4:0] pipeline_commit_wr_idx,   // Could be used to relay to ROB.
    output logic [`XLEN-1:0] pipeline_commit_wr_data,
    output logic pipeline_commit_wr_en,
    output logic [`XLEN-1:0] pipeline_commit_NPC,


    // testing hooks (data used to evalulate synthesized version) data is tested by looking at
    // final values in memory ( and ig the pipeline registers)

    // Outputs from IF-Stage
    output logic [`XLEN-1:0] if_NPC_out, // Next PC out (I believe should be )
    output logic [31:0]      if_IR_out, // The instruction here
    output logic             if_valid_inst_out,


    // Outputs from IF/ID Pipeline Register
    output logic [`XLEN-1:0] if_id_NPC,
    output logic [31:0] if_id_IR,
    output logic if_id_valid_inst,


    // Outputs from ID/EX Pipeline Register
    output logic [`XLEN-1:0] id_ex_NPC,
    output logic [31:0]      id_ex_IR,
    output logic         id_ex_valid_inst,

    // Outputs from EX/MEM Pipeline Register
    output logic [`XLEN-1:0] ex_mem_NPC,
    output logic [31:0]      ex_mem_IR,
    output logic             ex_mem_valid_inst,

    // Outputs from MEM/WB Pipeline Register
    output logic [`XLEN-1:0] mem_wb_NPC,
    output logic [31:0]      mem_wb_IR,
    output logic         mem_wb_valid_inst
); // End of signals (inputs/ outputs)

    // Pipeline register enables
    logic  if_id_enable, id_ex_enable, ex_mem_enable, mem_wb_enable;

    // Inputs into IF-stage
    logic pc_en;

    // Outputs from IF-State
    logic [`XLEN-1:0] proc2Imem_addr;
    IF_ID_PACKET if_packet, if_packet_out;  // Consists of whether or not instruction is garbage and pc / npc ( and the instruction itself)

    // Outputs from IF/ID Pipeline Register
    IF_ID_PACKET if_id_packet;

    // Outputs from ID stage
    ID_EX_PACKET id_packet;
    ID_EX_PACKET id_packet_next;
    logic        interlock_req;

    // Outputs from ID/EX Pipeline Register
    ID_EX_PACKET id_ex_packet; // Need to stall on a load (caused by a LD in the EX stage)

    // Outputs from EX-Stage
    EX_MEM_PACKET ex_packet, ex_packet_out;

    // Outputs from EX/MEM Pipeline Register
    EX_MEM_PACKET ex_mem_packet;
    
    // Outputs from MEM-Stage
    logic [`XLEN-1:0] mem_result_out; // Results from either alu_result (if not a load) or memory (if load)
    logic [`XLEN-1:0] proc2Dmem_addr; // Why address?
    logic [`XLEN-1:0] proc2Dmem_data; // Should be for stores...
    logic [1:0] proc2Dmem_command; // The command (which should be some load, store perhaps)
    MEM_SIZE proc2Dmem_size; // What is the size of bus I believe.
    logic take_branch;

    // Outputs from MEM/WB Pipeline Register
    logic         mem_wb_halt;
    logic         mem_wb_illegal;
    logic [4:0]       mem_wb_dest_reg_idx;
    logic [`XLEN-1:0] mem_wb_result;
    logic         mem_wb_take_branch;

    // Outputs from WB-Stage (These loop back to the register file in ID)
    logic [`XLEN-1:0] wb_reg_wr_data_out;
    logic [4:0]   wb_reg_wr_idx_out;
    logic             wb_reg_wr_en_out;

    assign pipeline_completed_insts = { 3'b0, mem_wb_valid_inst };
    assign pipeline_error_status =  mem_wb_illegal             ? ILLEGAL_INST :
                                    mem_wb_halt                ? HALTED_ON_WFI :
                                    (mem2proc_response==4'h0)  ? LOAD_ACCESS_FAULT :
                                    NO_ERROR;
    assign pipeline_commit_wr_idx  = wb_reg_wr_idx_out;
    assign pipeline_commit_wr_data = wb_reg_wr_data_out;
    assign pipeline_commit_wr_en   = wb_reg_wr_en_out;
    assign pipeline_commit_NPC     = mem_wb_NPC;

    // Wires used as input for what to obtain / write to memory module (look at mem.sv for more info)
    assign proc2mem_command =
        ( proc2Dmem_command == BUS_NONE ) ? BUS_LOAD : proc2Dmem_command; // If nothing for memory, let us do an instruction load
    assign proc2mem_addr =
        ( proc2Dmem_command == BUS_NONE ) ? proc2Imem_addr : proc2Dmem_addr; // Load in address from instruction (pc supposedly) or else maintain current address.
    // if it's an instruction, then load a double word (64 bits)
    assign proc2mem_size = 
        ( proc2Dmem_command == BUS_NONE ) ? DOUBLE : proc2Dmem_size;
    
    assign proc2mem_data = { 32'b0, proc2Dmem_data };


    // MEM_stage assignments
    assign take_branch = ex_mem_packet.valid & ex_mem_packet.take_branch;

//////////////////////////////////////////////////
//                                              //
//                  IF-Stage                    //
//                                              //
//////////////////////////////////////////////////
    
    // these are debug signals that are now included in the packet,
    //  breaking them out to support the legacy debug modes
    // driving output signals for the if-stage
    //! Remember that if_packet represents the state of the if-piepline
    // stage
    assign if_NPC_out        = if_packet.NPC;
    assign if_IR_out         = if_packet.inst;
    assign if_valid_inst_out = if_packet.valid;
    // Structural hazard here ( if proc2Dmem_command != BUS_NONE )
    // Other being that a stall is required
    assign pc_en = !interlock_req & proc2Dmem_command == BUS_NONE;

    if_stage if_stage_0 (
        // Inputs
        .clock( clock ),
        .reset ( reset ),
        .pc_en( pc_en ), // Disable pc_reg if low (for cycle we need to stall) ( unless there's a branch )
        .ex_mem_take_branch( take_branch ),
        .ex_mem_target_pc( ex_mem_packet.alu_result ), // The calculated branch (based off of alu results)
        .Imem2proc_data( mem2proc_data ), // Data coming back from memory (could be the instruction cache)

        // Outputs
        .proc2Imem_addr( proc2Imem_addr ), // address of instruction

        .if_packet_out( if_packet_out ) // Fetched instruction (read from memory, pc + 1, etc.)
    );

    // Change if_packet if pc_en == 1'b0
    always_comb
    begin
            if ( !pc_en )
            begin
                    if_packet = `NOP;
                    if_packet.valid = `FALSE; // As to not be counted as a valid instruction (in CPI)
            end
            else
                    if_packet = if_packet_out;
    end

//////////////////////////////////////////////////
//                                              //
//            IF/ID Pipeline Register           //
//                                              //
//////////////////////////////////////////////////

    // Output signals relating to if/id pipeline registers (aka just
        // copying
    assign if_id_NPC    = if_id_packet.NPC;
    assign if_id_IR     = if_id_packet.inst;
    assign if_id_valid_inst = if_id_packet.valid;
    assign if_id_enable = !interlock_req || take_branch; // prioritize enabling in order to change instruction to invalid (on take_branch). O.w. may forget that interlock_req on the next cycle.
    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) 
    begin
        if ( reset ) 
        begin
            if_id_packet.inst <= `SD `NOP;
            if_id_packet.valid <= `SD `FALSE;

        end 
        else // Normal part
        begin
            if ( if_id_enable ) // Update the pc / next pc state
            begin
                if ( take_branch ) // If branches are being taken or, we need to turn instruction into NOOP
                begin
                        if_id_packet.inst <= `NOP;
                        if_id_packet.valid <= `FALSE;
                end
		    else
                	if_id_packet <= `SD if_packet;
            end // if ( if_id_enable )
            else
            begin
                if_id_packet <= if_id_packet; // We want to keep the instruction (without changing)
            end // else
        end
    end // end always

    // We expect when mem_wb_take_branch is high, if_id_packet.valid is low.
    // I chose mem_wb_take_branch since it should be easy to verify that IF/ID, ID/EX, and EX/MEM
    // should all have invalidated instructions.
    assert property( @( negedge clock ) mem_wb_valid_inst && mem_wb_take_branch |-> !if_id_packet.valid ); 

//////////////////////////////////////////////////
//                                              //
//                  ID-Stage                    //
//                                              //
//////////////////////////////////////////////////
    
    id_stage id_stage_0 ( // Inputs
        .clock( clock ),
        .reset( reset ),
        .if_id_packet_in    ( if_id_packet ),       // pipeline results from IF stage into decode (for actual decoding)
        .wb_reg_wr_en_out   ( wb_reg_wr_en_out ),   // Is write enabled?
        .wb_reg_wr_idx_out  ( wb_reg_wr_idx_out ),  // The index in which to write to in the register file.
        .wb_reg_wr_data_out ( wb_reg_wr_data_out ), // Outputs from wb stage (data being written to register file)
	.take_branch( take_branch ),

        // Outputs
        .id_packet_out      ( id_packet ),
        .interlock_req      ( interlock_req )
    );

    // I have yet to figure out how to do this without needing additional
    // intermediate wires (aka doing this in the always loop) -> Turns out doing an else statement when latching will allow it to be properly written to.
    always_comb
    begin
        id_packet_next = id_packet;
        if ( interlock_req || take_branch ) // Invalidate instruction
	    begin
            id_packet_next.valid   = `FALSE;
            id_packet_next.halt    = `FALSE; // Not making false will cause the pipeline to prematurely end.
            id_packet_next.illegal = `FALSE; // This may have happened (due to going off of the end)
	    end
    end
         
//////////////////////////////////////////////////
//                                              //
//            ID/EX Pipeline Register           //
//                                              //
//////////////////////////////////////////////////

    assign id_ex_NPC            = id_ex_packet.NPC;
    assign id_ex_IR             = id_ex_packet.inst;
    assign id_ex_valid_inst     = id_ex_packet.valid;

    assign id_ex_enable = `TRUE; // always enabled

    // synopsys sync_set_reset "reset"
    always_ff @( posedge clock )
    begin
       if ( reset ) 
       begin
               id_ex_packet <= `SD { {`XLEN{1'b0} },   // NPC
                                     {`XLEN{1'b0} },   // PC
                                     {`XLEN{1'b0} },   // rs1_value
                                     {`XLEN{1'b0} },   // rs2_value
                                     OPA_IS_RS1,       // opa_select
                                     OPB_IS_RS2,       // opb_select
                                     `NOP,             // inst
                                     `ZERO_REG,        // dest_reg_idx
                                     ALU_ADD,         // ALU_FUNC
                                     1'b0,             // rd_mem
                                     1'b0,             // wr_mem
                                     1'b0,             // cond_branch
                                     1'b0,             // uncond_branch
                                     1'b0,             // halt
                                     1'b0,             // illegal
                                     1'b0,             // csr_op
                                     1'b0,             // valid
                                     NO_BYPASS,        // frwda_sel
                                     NO_BYPASS         // frwdb_sel
                                   };
       end // if ( reset )
       else
       begin
               if ( id_ex_enable ) // Always enabled
               begin
                       id_ex_packet <= `SD id_packet_next;
                       //if ( interlock_req || ex_mem_packet.take_branch ) // Invalidate instruction (due to stall)
               end // if ( id_ex_enable )
       end // else
    end // always_ff( )

    assert property( @( negedge clock ) mem_wb_valid_inst && mem_wb_take_branch |-> !id_ex_packet.valid ); 
    

//////////////////////////////////////////////////
//                                              //
//                  EX-Stage                    //
//                                              //
//////////////////////////////////////////////////
    ex_stage ex_stage_0 (
            // Inputs
            .clock( clock ),
            .reset( reset ),
            .id_ex_packet_in( id_ex_packet ),
            .ex_mem_fwrd( ex_mem_packet.alu_result  ),
            .mem_wb_fwrd( mem_wb_result ),

            // Outputs
            .ex_packet_out( ex_packet_out )
        );

    always_comb
    begin
        ex_packet = ex_packet_out;
        if ( take_branch )
	    begin
            ex_packet.valid   = `FALSE;
	    ex_packet.halt    = `FALSE;
            ex_packet.illegal = `FALSE;
	    end
    end

//////////////////////////////////////////////////
//                                              //
//           EX/MEM Pipeline Register           //
//                                              //
//////////////////////////////////////////////////

    assign ex_mem_NPC         = ex_mem_packet.NPC;
    assign ex_mem_valid_inst = ex_mem_packet.valid;

    assign ex_mem_enable      = 1'b1; // always enabled
    // synopsys sync_set_reset "reset"
    always_ff @( posedge clock )
    begin
        if ( reset )
        begin
                ex_mem_IR   <= `SD `NOP;
                ex_mem_packet <= `SD 0;
        end // if ( reset
        else
        begin
                if ( ex_mem_enable )
                begin
                    // tese are forwarded directory from IR/EX registers, only for debugging purposes
                    ex_mem_IR     <= `SD id_ex_IR;
                    // EX outputs
                    ex_mem_packet <= `SD ex_packet;                    
                end // if ( ex_mem_enable )
        end // else
    end // always_ffif_id_packet

    assert property( @( negedge clock ) mem_wb_valid_inst && mem_wb_take_branch |-> !ex_mem_packet.valid ); 
//////////////////////////////////////////////////
//                                              //
//                 MEM-Stage                    //
//                                              //
//////////////////////////////////////////////////

    mem_stage mem_stage_0 ( // Inputs
            .clock( clock ),
            .reset( reset ),
            .ex_mem_packet_in( ex_mem_packet ),
            .Dmem2proc_data( mem2proc_data[`XLEN - 1:0 ] ),


            // Outputs
            .mem_result_out( mem_result_out ), // For writeback
            .proc2Dmem_command( proc2Dmem_command ), // Going to memory
            .proc2Dmem_size( proc2Dmem_size ),
            .proc2Dmem_addr( proc2Dmem_addr ),
            .proc2Dmem_data( proc2Dmem_data )
        );
       

//////////////////////////////////////////////////
//                                              //
//           MEM/WB Pipeline Register           //
//                                              //
//////////////////////////////////////////////////
    assign  mem_wb_enable = 1'b1; // always enabled (as expected)
    // synopsys sync_set_reset "reset"
    always_ff @( posedge clock )
    begin
           if ( reset )
           begin
                   mem_wb_NPC            <= `SD 0;
                   mem_wb_IR             <= `SD `NOP;
                   mem_wb_halt           <= `SD 0;
                   mem_wb_illegal        <= `SD 0;
                   mem_wb_valid_inst     <= `SD 0;
                   mem_wb_dest_reg_idx   <= `SD `ZERO_REG;
                   mem_wb_take_branch    <= `SD 0;
                   mem_wb_result         <= `SD 0;
           end // if ( reset )
           else
           begin
                   if ( mem_wb_enable )
                   begin
                           mem_wb_NPC            <= `SD ex_mem_packet.NPC;
                           mem_wb_IR             <= `SD ex_mem_IR;
                           mem_wb_halt           <= `SD ex_mem_packet.halt; 
                           mem_wb_illegal        <= `SD ex_mem_packet.illegal;
                           mem_wb_valid_inst     <= `SD ex_mem_packet.valid;
                           mem_wb_dest_reg_idx   <= `SD ex_mem_packet.dest_reg_idx;
                           mem_wb_take_branch    <= `SD ex_mem_packet.take_branch;
                           mem_wb_result         <= `SD mem_result_out;
                   end // if
           end // else
    end // always_ff

    // Only halts when halt instruction is also valid.
    assert property( @( negedge clock ) mem_wb_halt |-> ex_mem_packet.valid );

//////////////////////////////////////////////////
//                                              //
//                  WB-Stage                    //
//                                              //
//////////////////////////////////////////////////
    wb_stage wb_stage_0 (   
            // Inputs
            .clock( clock ),
            .reset( reset ),
            .mem_wb_NPC( mem_wb_NPC ),
            .mem_wb_result( mem_wb_result ),
            .mem_wb_dest_reg_idx(mem_wb_dest_reg_idx),
            .mem_wb_take_branch(mem_wb_take_branch),
            .mem_wb_valid_inst(mem_wb_valid_inst),

            // Outputs
            .reg_wr_data_out( wb_reg_wr_data_out ),
            .reg_wr_idx_out( wb_reg_wr_idx_out ),
            .reg_wr_en_out( wb_reg_wr_en_out )
        );
endmodule // module pipeline 
`endif // __PIPELINE_V__


    


