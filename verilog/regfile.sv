

// Reimplementation of registe file

`ifndef __REGFILE_V__
`define __REGFILE_V__

`timescale 1ns/100ps

module regfile(
	input [4:0]         rda_idx, rdb_idx, wr_idx, // read/write index
	input [`XLEN-1:0]   wr_data,
	input 		    wr_en, wr_clk,

	output logic [`XLEN-1:0] rda_out, rdb_out // read_data

);

	// 32 interger registers (this would imply 64-bit registers)
	// This is a multidimensional packed array with a set of contigous bits but also able to be segmented into smaller groups
	logic [31:0] [ `XLEN-1:0 ] registers; // 32, 32-bit register

	wire [`XLEN-1:0] rda_reg = registers[rda_idx];
	wire [`XLEN-1:0] rdb_reg = registers[rdb_idx];


	// Read port A
	read_port port_a( .rd_idx( rda_idx ), .wr_idx( wr_idx ), 
		.wr_data( wr_data ), .rd_reg( rda_reg ), .wr_en( wr_en ), .rd_out( rda_out ) );

	// Read port B
	read_port port_b( .rd_idx( rdb_idx ), .wr_idx( wr_idx ), 
		.wr_data( wr_data ), .rd_reg( rdb_reg ), .wr_en( wr_en ), .rd_out( rdb_out ) );

	// Write port
	always_ff @( posedge wr_clk )
	begin
		if ( wr_en ) begin
			registers[ wr_idx ] <= `SD wr_data;
		end
	end
endmodule

module read_port( 
	input [4:0] rd_idx, wr_idx,
	input [`XLEN-1:0] wr_data, rd_reg,
	input wr_en,

	output logic [`XLEN-1:0] rd_out
);

	always_comb
	begin
		if ( rd_idx == `ZERO_REG )
			rd_out = 0;
		else if ( wr_en && ( wr_idx == rd_idx ) )
			rd_out = wr_data; // Internal forwarding
		else
			rd_out = rd_reg;
	end
endmodule
`endif
