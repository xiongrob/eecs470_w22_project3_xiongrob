// Modulename: id_stage.v
//
//
// Description: instruciton decode (ID) stage of the pipeline;
//              decode the instruction fet register operands, and
//              comoute immediate operand (if applicable)


`timescale 1ns/100ps

  // Decode an instruction: given instruction bits IR produce the 
  // approipate deatapth control signals.
  //
  // This is a "combinational" modulea (basically a PLA).
/*
module decoder(
        input IF_ID_PACKET if_packet,
        
        output ALU_OPA_SELECT opa_select,
        output ALU_OPB_SELECT
);

endmodule
*/
// rs1_needed and rs2_needed are bits used to indciate that this instruction
// requires the contents of it (so bypassing may or may not be required).
module decoder(

	//input [31:0] inst,
	//input valid_inst_in,  // ignore inst when low, outputs will
	                      // reflect noop (except valid_inst)
	//see sys_defs.svh for definition
	input IF_ID_PACKET if_packet,
	
	output ALU_OPA_SELECT opa_select,
	output ALU_OPB_SELECT opb_select,
	output DEST_REG_SEL   dest_reg, // mux selects
	output ALU_FUNC       alu_func,
	output logic rd_mem, wr_mem, cond_branch, uncond_branch, rs1_needed, rs2_needed,
	output logic csr_op,          // used for CSR operations, we only used this as 
	                              // a cheap way to get the return code out
	output logic halt,            // non-zero on a halt
	output logic illegal,         // non-zero on an illegal instruction
	output logic valid_inst       // for counting valid instructions executed
	                              // and for making the fetch stage die on halts/
	                              // keeping track of when to allow the next
	                              // instruction out of fetch
	                              // 0 for HALT and illegal instructions (die on halt)
);

	INST inst;
    logic valid_inst_in; // ignore inst when low, ouputs will reflect noop

	assign inst          = if_packet.inst;
	assign valid_inst_in = if_packet.valid;
	assign valid_inst    = valid_inst_in & ~illegal;
	
	always_comb begin
		// default control values:
		// - valid instructions must override these defaults as necessary.
		//	 opa_select, opb_select, and alu_func should be set explicitly.
		// - invalid instructions should clear valid_inst.
		// - These defaults are equivalent to a noop
		// * see sys_defs.vh for the constants used here
		opa_select = OPA_IS_RS1;
		opb_select = OPB_IS_RS2;
		alu_func = ALU_ADD;
		dest_reg = DEST_NONE;
		csr_op = `FALSE;
		rd_mem = `FALSE;
		wr_mem = `FALSE;
		cond_branch = `FALSE;
		uncond_branch = `FALSE;
		rs1_needed = `TRUE; // Starting out true for both means less changes required (most instructions's addressing modes are registers since U-type, I-type and load S-tyeps doesn't use rs2.
		rs2_needed = `TRUE;
		halt = `FALSE;
		illegal = `FALSE;
		if(valid_inst_in) begin
			casez (inst) 
				`RV32_LUI: begin
					dest_reg   = DEST_RD;
					opa_select = OPA_IS_ZERO;
					opb_select = OPB_IS_U_IMM;
					rs1_needed = `FALSE; // U-type instructions don't use either registers.
					rs2_needed = `FALSE;
				end
				`RV32_AUIPC: begin
					dest_reg   = DEST_RD;
					opa_select = OPA_IS_PC;
					opb_select = OPB_IS_U_IMM;
					rs1_needed = `FALSE;
					rs2_needed = `FALSE;
				end
				`RV32_JAL: begin
					dest_reg      = DEST_RD;
					opa_select    = OPA_IS_PC;
					opb_select    = OPB_IS_J_IMM;
					uncond_branch = `TRUE;
					rs1_needed = `FALSE;
					rs2_needed = `FALSE;
				end
				`RV32_JALR: begin // Jump and link register (uses rs1 as base but no need for rs2). Though rd is used as the link register.
					dest_reg      = DEST_RD;
					opa_select    = OPA_IS_RS1;
					opb_select    = OPB_IS_I_IMM;
					uncond_branch = `TRUE;
					rs2_needed = `FALSE;
				end
				`RV32_BEQ, `RV32_BNE, `RV32_BLT, `RV32_BGE,
				`RV32_BLTU, `RV32_BGEU: begin // All B* insturctions use both rs1 and rs2 as operands.
					opa_select  = OPA_IS_PC;
					opb_select  = OPB_IS_B_IMM;
					cond_branch = `TRUE;
				end
				`RV32_LB, `RV32_LH, `RV32_LW,
				`RV32_LBU, `RV32_LHU: begin
					dest_reg   = DEST_RD;
					opb_select = OPB_IS_I_IMM;
					rd_mem     = `TRUE;
					rs2_needed = `FALSE; // rs2 doesn't exist (only cares about rs1 (base) and rd (dest)).
				end
				`RV32_SB, `RV32_SH, `RV32_SW: begin
					opb_select = OPB_IS_S_IMM;
					wr_mem     = `TRUE;
				end
				`RV32_ADDI: begin
					dest_reg   = DEST_RD;
					opb_select = OPB_IS_I_IMM;
					rs2_needed = `FALSE; // Is intermidate instruction.
				end
				`RV32_SLTI: begin
					dest_reg   = DEST_RD;
					opb_select = OPB_IS_I_IMM;
					alu_func   = ALU_SLT;
					rs2_needed = `FALSE;
				end
				`RV32_SLTIU: begin
					dest_reg   = DEST_RD;
					opb_select = OPB_IS_I_IMM;
					alu_func   = ALU_SLTU;
					rs2_needed = `FALSE;
				end
				`RV32_ANDI: begin
					dest_reg   = DEST_RD;
					opb_select = OPB_IS_I_IMM;
					alu_func   = ALU_AND;
					rs2_needed = `FALSE;
				end
				`RV32_ORI: begin
					dest_reg   = DEST_RD;
					opb_select = OPB_IS_I_IMM;
					alu_func   = ALU_OR;
					rs2_needed = `FALSE;
				end
				`RV32_XORI: begin
					dest_reg   = DEST_RD;
					opb_select = OPB_IS_I_IMM;
					alu_func   = ALU_XOR;
					rs2_needed = `FALSE;
				end
				`RV32_SLLI: begin
					dest_reg   = DEST_RD;
					opb_select = OPB_IS_I_IMM;
					alu_func   = ALU_SLL;
					rs2_needed = `FALSE;
				end
				`RV32_SRLI: begin // Shfit right logical (immediate)
					dest_reg   = DEST_RD;
					opb_select = OPB_IS_I_IMM;
					alu_func   = ALU_SRL;
					rs2_needed = `FALSE;
				end
				`RV32_SRAI: begin // Shift right Arithmetic
					dest_reg   = DEST_RD;
					opb_select = OPB_IS_I_IMM;
					alu_func   = ALU_SRA;
					rs2_needed = `FALSE;
				end
				`RV32_ADD: begin
					dest_reg   = DEST_RD;
				end
				`RV32_SUB: begin
					dest_reg   = DEST_RD;
					alu_func   = ALU_SUB;
				end
				`RV32_SLT: begin // Set Less-than (signed)
					dest_reg   = DEST_RD;
					alu_func   = ALU_SLT;
				end
				`RV32_SLTU: begin
					dest_reg   = DEST_RD;
					alu_func   = ALU_SLTU;
				end
				`RV32_AND: begin
					dest_reg   = DEST_RD;
					alu_func   = ALU_AND;
				end
				`RV32_OR: begin
					dest_reg   = DEST_RD;
					alu_func   = ALU_OR;
				end
				`RV32_XOR: begin
					dest_reg   = DEST_RD;
					alu_func   = ALU_XOR;
				end
				`RV32_SLL: begin
					dest_reg   = DEST_RD;
					alu_func   = ALU_SLL;
				end
				`RV32_SRL: begin
					dest_reg   = DEST_RD;
					alu_func   = ALU_SRL;
				end
				`RV32_SRA: begin
					dest_reg   = DEST_RD;
					alu_func   = ALU_SRA;
				end
				`RV32_MUL: begin
					dest_reg   = DEST_RD;
					alu_func   = ALU_MUL;
				end
				`RV32_MULH: begin
					dest_reg   = DEST_RD;
					alu_func   = ALU_MULH;
				end
				`RV32_MULHSU: begin
					dest_reg   = DEST_RD;
					alu_func   = ALU_MULHSU;
				end
				`RV32_MULHU: begin
					dest_reg   = DEST_RD;
					alu_func   = ALU_MULHU;
				end
				`RV32_CSRRW, `RV32_CSRRS, `RV32_CSRRC: begin
					csr_op = `TRUE;
					rs2_needed = `FALSE;
				end
				`WFI: begin
					halt = `TRUE;
					rs1_needed = `FALSE;
					rs2_needed = `FALSE;
				end
				default: illegal = `TRUE;

		endcase // casez (inst)
		end // if(valid_inst_in)
	end // always
endmodule // decoder

typedef struct packed {
        logic [4:0] dest_reg_idx;
} DEST_REGS;

module  dep_to_sel (
        input [1:0] [4:0] dests,    // The current indx of dest registers for the next two stages (EX and MEM)
        input [4:0] rd_idx,
        input rs_needed,             // Is the "src" register in question 
        output BYPASS_SELECT frwd_sel
);
    logic dep_w_ex_stage, chk_req;
    // It automatically shouldn't matter if rd_idx is the 'ZERO_REG (no dependences)
    assign chk_req = rs_needed && rd_idx != `ZERO_REG; // In this case, we need to check for dependencies
    assign dep_w_ex_stage = rd_idx == dests[0]; // This proritizes data from the ex-stage.
    assign frwd_sel[0] = chk_req & dep_w_ex_stage;
    assign frwd_sel[1] = chk_req && !dep_w_ex_stage && rd_idx == dests[1];
endmodule

module hazard_detector( 
        input [1:0] [4:0] dests,    // The current indx of dest registers for the next two stages (EX and MEM)
        input [4:0] rda_idx, rdb_idx,         // The src register's index
        input ex_stage_ld,              // Does the ex_stage have a load instruction?
	input rs1_needed, rs2_needed,	// Does this instruction need these src registers?
        input inst_valid,   		// Is this instruction a valid one
        output BYPASS_SELECT frwda_sel, frwdb_sel,    // The bits used to select which data to use as input.
        output interlock_req            // Based off of dependency on load
);
    dep_to_sel rda( .dests( dests ), .rd_idx( rda_idx ), .rs_needed( rs1_needed ), .frwd_sel( frwda_sel ) );
    dep_to_sel rdb( .dests( dests ), .rd_idx( rdb_idx ), .rs_needed( rs2_needed ), .frwd_sel( frwdb_sel ) );

    // frwda_sel[0] being high implies that there's a RAW with ex_stage (next instruction).
    // Is the next instruction a load inst?
    assign interlock_req = ( frwda_sel[0] | frwdb_sel[0] ) & ex_stage_ld & inst_valid;
endmodule


module id_stage(
        input clock,
        input reset,
        input IF_ID_PACKET if_id_packet_in,
        input wb_reg_wr_en_out,
        input [4:0] wb_reg_wr_idx_out,
        input [`XLEN-1:0] wb_reg_wr_data_out,
	input take_branch,
        
        output ID_EX_PACKET id_packet_out, // The signals to be sent to the EX stage
        output interlock_req               // Stall is required.
);
    // Keeps track of the dest registers (for forwarding) ( we only care about
    // two since the third one is handled via (wb_reg_wr_idx_out)
    logic [1:0] [4:0] dests;
    logic ex_stage_is_ld;
    logic rs1_needed, rs2_needed; // Bits telling forwarding logic whether or not to even consider bypassing (only if high).

    hazard_detector hazard_det( .dests( dests ), .rda_idx( if_id_packet_in.inst.r.rs1 ), 
                                                 .rdb_idx( if_id_packet_in.inst.r.rs2 ), 
                                                 .ex_stage_ld( ex_stage_is_ld ), 
						 .rs1_needed ( rs1_needed ),
						 .rs2_needed ( rs2_needed ),
						 .inst_valid( id_packet_out.valid ),
                                                 .frwda_sel( id_packet_out.frwda_sel ), .frwdb_sel( id_packet_out.frwdb_sel ),
                                                 .interlock_req( interlock_req ) ); 


    assign id_packet_out.inst = if_id_packet_in.inst;
    assign id_packet_out.NPC  = if_id_packet_in.NPC;
    assign id_packet_out.PC   = if_id_packet_in.PC;
    // Wire used to figure out what to output to
    /*
     * typedef enum logic [1:0] {
     *      DEST_RD   = 2'h0,
     *      DEST_NONE = 2'h1
     * } DEST_REG_SEL;
     * 
    */

    // Does this instruction write back to the regfile via some destination
    // register?
    DEST_REG_SEL dest_reg_sel_decoder, dest_reg_select;
	
    // Instantiate the register file used by this pipeline
    regfile regf_0 (
        // inputs
        .rda_idx( if_id_packet_in.inst.r.rs1 ), // fixed-field decoding
        .rda_out( id_packet_out.rs1_value ),

        .rdb_idx( if_id_packet_in.inst.r.rs2 ),
        .rdb_out( id_packet_out.rs2_value ),

        .wr_clk( clock ),
        .wr_en( wb_reg_wr_en_out ),
        .wr_idx( wb_reg_wr_idx_out ),

        // Output
        .wr_data( wb_reg_wr_data_out )
    );

    // instantiate the instruction decoder

    decoder decoder_0 (
        .if_packet( if_id_packet_in ),
        
        // Outputs
        .opa_select( id_packet_out.opa_select ),
        .opb_select( id_packet_out.opb_select ),
        .dest_reg( dest_reg_sel_decoder ), // Does this inst have a dest reg
        .alu_func( id_packet_out.alu_func ), // What is the alu function?
        .rd_mem( id_packet_out.rd_mem ),
        .wr_mem( id_packet_out.wr_mem ),
        .cond_branch( id_packet_out.cond_branch ),
        .uncond_branch( id_packet_out.uncond_branch ),
	.rs1_needed ( rs1_needed ),
	.rs2_needed ( rs2_needed ),
        .csr_op( id_packet_out.csr_op ),
        .halt( id_packet_out.halt ),
        .illegal( id_packet_out.illegal ),
        .valid_inst( id_packet_out.valid )
    );

    // mux to generate dest_reg_idx based on
    // the dest_reg_select output from decoder
    always_comb
    begin
	    // Needs to not write to dest_reg_idx in this case (ow forwarding
	    // logic will be incorrect).
	    if ( interlock_req || take_branch )
		    dest_reg_select = DEST_NONE; // NEed to ensure forwarding logic isn't ruined by the instruction being stalled for a cycle. 
	    else
		    dest_reg_select = dest_reg_sel_decoder; // Just proceed as normal
            case( dest_reg_select )
                    DEST_RD:                id_packet_out.dest_reg_idx = if_id_packet_in.inst.r.rd;
                    DEST_NONE:              id_packet_out.dest_reg_idx = `ZERO_REG;
		    default:                id_packet_out.dest_reg_idx = `ZERO_REG;
            endcase // case( dest_reg_select )

    end

    // on the posedge, shift registers (for dest registers)
    always_ff @( posedge clock )
    begin
            dests <= `SD { dests[0], id_packet_out.dest_reg_idx }; // Shift in dest_reg_idx
            ex_stage_is_ld <= `SD id_packet_out.rd_mem & id_packet_out.valid; // is true iff rd_mem (aka is this a load?) and (not just some stalled instr?)
    end // always_ff

endmodule
