/*
	TEST PROGRAM #4: compute nth fibonacci number recursively

	int output;
	
	void
	main(void)
	{
	   output = fib(14); 
	}

	int
	fib(int arg)
	{
	    if (arg == 0 || arg == 1)
		return 1;

	    return fib(arg-1) + fib(arg-2);
	}
*/
	
	data = 0x400
	stack = 0x1000
    li  x8, 1
	li	x31, stack
	
	li	x17, 14 	# Pass 14 into fib
	jal	x27,	fib     # Call fib(14)

	li	x2, data
	sw	x1, 0(x2)	
	wfi
	
fib:	beq	x17,	x0,	fib_ret_1 # arg is 0: return 1

	#cmpeq	x2,	x17,	1 # arg is 1: return 1
	beq	x17,	x8,	fib_ret_1 # arg is 1: return 1

	addi	x31,	x31,	-32 # allocate stack frame (32 bytes)
	sw	x27, 24(x31)	    # Store the return address (in x27) -> Store 8 bytes!

	sw	x17, 0(x31)	    # Store the argument ( we need it for fib( arg-2) )

	addi	x17,	x17,	-1 # arg = arg-1
	jal	x27,	fib # call fib
	sw	x1, 8(x31)	

	lw	x17, 0(x31)	
	addi	x17,	x17,	-2 # arg = arg-2

	# This following line has something unique. A branch followed by a supposed interlock due to a dependency RAW load dependency on add. Improper logic doesn't priorize clearing the pipeline despite such dependency leading to instruction staying in pipeline (as it ins't enabled to clear it on take_branch).
	jal	x27,	fib # call fib

	lw	x2, 8(x31)	
	add	x1,	x2,	x1 # fib(arg-1)+fib(arg-2)

	lw	x27, 24(x31)	
	addi	x31,	x31,	32 # deallocate stack frame
	jalr x0, x27, 0
	
fib_ret_1:
	li	x1,	1 # set return value to 1
	jalr x0, x27, 0
	
